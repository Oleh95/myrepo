package com.nmu.zinchenko;

/**
 * Created by Oleh on 22.11.2016.
 */
public class Student {
    String name;
    String surname;
    Integer age;
    OrderBook ob;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (!name.equals(student.name)) return false;
        if (!surname.equals(student.surname)) return false;
        if (!age.equals(student.age)) return false;
        return ob.equals(student.ob);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + age.hashCode();
        result = 31 * result + ob.hashCode();
        return result;
    }
}
