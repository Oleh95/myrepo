package com.nmu.zinchenko;

import java.io.*;

/**
 * Created by Oleh on 22.11.2016.
 */
public class DemoClass {
    public static void Write(String filename, String text)
    {
        File file= new File(filename);
        try {
                if (!file.exists())
                {
                file.createNewFile();
                }
            PrintWriter pw= new PrintWriter(file.getAbsolutePath());
            pw.print(text);
        }
                catch (IOException e)
                {
                e.printStackTrace();
                }
    }

    public static String Read(String filename)
    {
        StringBuilder sb= new StringBuilder();
        File file= new File(filename);
        try {
            if (!file.exists())
                return null;
            FileReader fr = new FileReader(file.getAbsolutePath());
            BufferedReader br = new BufferedReader(fr);
            String result;
            while ((result=br.readLine())!=null)
            {
                sb.append(result);
                sb.append('\n');
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    return sb.toString();

    }
}
