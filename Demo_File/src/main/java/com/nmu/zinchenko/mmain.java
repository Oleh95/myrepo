package com.nmu.zinchenko;

import com.google.gson.Gson;

/**
 * Created by Oleh on 22.11.2016.
 */
public class mmain {
    public static void main(String[] args) {
        Student st=new Student();
        st.name="Ololo";
        st.surname="Trololo";
        st.age=150;
        OrderBook ob=new OrderBook();
        ob.name="Zachetka";
        ob.number=1;
        st.ob=ob;
        Gson gson= new Gson();
        String json=gson.toJson(st);
        System.out.println(json);
        Student studentFromJson= new Student();
        gson.fromJson(json,Student.class);
        //System.out.println(studentFromJson);
        System.out.println(st.equals(studentFromJson));
    }
}
