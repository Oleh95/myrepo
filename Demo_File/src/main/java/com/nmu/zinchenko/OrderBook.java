package com.nmu.zinchenko;

/**
 * Created by Oleh on 22.11.2016.
 */
public class OrderBook {
    Integer number;
    String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderBook orderBook = (OrderBook) o;

        if (!number.equals(orderBook.number)) return false;
        return name.equals(orderBook.name);

    }

    @Override
    public int hashCode() {
        int result = number.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }
}
