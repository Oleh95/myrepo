package com.nmu.zinchenko;

import com.google.gson.Gson;

/**
 * Created by Oleh on 04.12.2016.
 */
public class main {
    public static void main(String[] args) {
        Car car= new Car();
        car.brand="BMW";
        car.model="model_5";
        car.price= 100000;
        Dealer dealer=new Dealer();
        dealer.name="Bavaria";
        dealer.branch=50;
        car.dealer=dealer;
        Gson gson=new Gson();
        String json=gson.toJson(car);
        System.out.println(json);
        Car studentFromJson= new Car();
        gson.fromJson(json,Car.class);
        //System.out.println(studentFromJson);
        //System.out.println(car.equals(studentFromJson));
    }
}
