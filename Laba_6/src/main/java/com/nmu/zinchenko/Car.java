package com.nmu.zinchenko;

/**
 * Created by Oleh on 04.12.2016.
 */
public class Car {
    String brand;
    String model;
    Integer price;
    Dealer dealer;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (!brand.equals(car.brand)) return false;
        if (!model.equals(car.model)) return false;
        if (!price.equals(car.price)) return false;
        return dealer.equals(car.dealer);

    }

    @Override
    public int hashCode() {
        int result = brand.hashCode();
        result = 31 * result + model.hashCode();
        result = 31 * result + price.hashCode();
        result = 31 * result + dealer.hashCode();
        return result;
    }
}
