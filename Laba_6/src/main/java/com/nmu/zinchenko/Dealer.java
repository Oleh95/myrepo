package com.nmu.zinchenko;

/**
 * Created by Oleh on 04.12.2016.
 */
public class Dealer {
    String name;
    Integer branch;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dealer dealer = (Dealer) o;

        if (!name.equals(dealer.name)) return false;
        return name.equals(dealer.name);
    }

    @Override
    public int hashCode() {
        int result = branch.hashCode();
        result = 31 * result + branch.hashCode();
        return result;
    }
}
