package com.nmu.zinchenko.matrix;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        /*Scanner in = new Scanner(System.in);
        int r,c;
        System.out.println("Please, enter size of matrix:");
        System.out.print("Row: ");
        r= Integer.parseInt(in.next());
        System.out.print("Column: ");
        c= Integer.parseInt(in.next());
        ObjMatrix matr= new ObjMatrix(r,c);
        matr.Print();
        System.out.println("Maximum "+matr.FindMax());
        System.out.println("Minimum "+matr.FindMin());
        //------------------ArrayList------------------
        ArrayList<String> list1=new ArrayList<String>();
        ArrayList list2=new ArrayList();
        ArrayList[] list=new ArrayList[]{list1,list2};
        list1.add(0,"BMW");
        list1.add(1,"Fiat");
        list1.add(2,"Porshe");
        list2.add(0,"X5");
        list2.add(1,"Uno");
        list2.add(2,"Cayman");
        List<List<String>> list=new ArrayList();
        list.add(1,list1.get(0));*/

        ArrayList<ArrayList<Integer>> outer = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer> inner = new ArrayList<Integer>();

        inner.add(100);
        inner.add(200);
        outer.add(inner); // add first list
        inner = new ArrayList<Integer>(inner); // create a new inner list that has the same content as
        // the original inner list
        outer.add(inner); // add second list

        outer.get(0).add(300); // changes only the first inner list

        System.out.println(outer);
    }
}
