package com.nmu.zinchenko.matrix;
import java.util.Random;

/**
 * Created by Oleh on 07.10.2016.
 */
public class ObjMatrix {
    private int row;
    private int column;
    private int[][] data;

    public ObjMatrix(int M,int N){
        this.row=M;
        this.column=N;
        data=new int[M][N];
        Random rand= new Random();
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                data[i][j] = rand.nextInt(100);
            }
        }
    }

    public void Print(){
        System.out.print("Size of matrix: "+row+" "+column);
        System.out.println();
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                System.out.print(data[i][j] + "\t");
            }
            System.out.println();
        }
    }
    public int FindMax(){
        int Row=0,Column=0;
        int MyMax=0;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if (data[i][j] > MyMax) {
                    MyMax = data[i][j];
                    Row=i; Column=j;
                }
            }
        }
        return MyMax;
    }
    public int FindMin(){
        int Row=0,Column=0;
        int MyMin=data[0][0];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if (data[i][j] < MyMin) {
                    MyMin = data[i][j];
                    Row=i; Column=j;
                }
            }
        }
        return MyMin;
    }
}

