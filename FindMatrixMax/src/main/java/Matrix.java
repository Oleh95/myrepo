/**
 * Created by Oleh on 23.09.2016.
 */
import java.util.Scanner;
import java.util.Random;

public class Matrix {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Size of matrix: ");
        Integer size = in.nextInt();
        int[][] matrixA;
        matrixA = new int[size][size];  //рандомное заполнение
        Random rand= new Random();

        //Заполняем
        /*for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print("matrix[" + i + "][" + j + "]=");
                matrixA[i][j] = in.nextInt();
            }
            System.out.println();
        }*/

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrixA[i][j] = rand.nextInt(100);
            }
        }
        System.out.print("-----------------------\n");
        //Стучимся к каждому элементу
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(matrixA[i][j] + "\t");
            }
            System.out.println();
        }
        //поиск максимума
        int Row=0,Column=0,MyMax=0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (matrixA[i][j] > MyMax) {
                    MyMax = matrixA[i][j];
                    Row=i; Column=j;
                }
            }
        }
        System.out.println("Maximum="+MyMax);
        System.out.println("Row="+Row+"\tColumn"+Column);
    }
}
